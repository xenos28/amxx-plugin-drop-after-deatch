#include <amxmodx>
#include <amxmisc>
#include <cstrike>
#include <engine>
#include <fakemeta>
#include <fakemeta_util>

#define MAX 32
#pragma semicolon 1

enum PluginSounds { psKasa, psApteczka, };
stock const gPluginSounds[ PluginSounds ][ 128 ] = { "dad/kasa.wav", "dad/apteczka.wav" };

enum PluginModels { pmKasa, pmApteczka, pmKamizelka, };
stock const gPluginModels[ PluginModels  ][ 128 ] = { "models/dad/w_kasa.mdl", "models/dad/w_apteczka.mdl", "models/w_kevlar.mdl" };

new const gMedKitClassname[ ] = "medkit_entity";

enum Msg { mScreenFade, mItemPickUp, };
new gMsg[ Msg ];
enum Cvar { cApteczka, cKasa, cArmor };
new gCvar[ Cvar ][ 4 ];

new moneybox[ 1500 ],  bool:can_pickup[ MAX + 1 ];
new Float:multiplier, g_ArmorType[ MAX + 1 ], Float:g_ArmorAmount[ MAX + 1 ], fArmor[ MAX + 1 ];

public plugin_init( ) {
	register_plugin( "Drop After Deatch", "1.0", "MisieQ" );
	
	
	gCvar[ cApteczka ][ 0 ] = register_cvar( "dad_apteczka_poswiata", "1" );
	gCvar[ cApteczka ][ 1 ] = register_cvar( "dad_apteczka_ekran", "1" );
	gCvar[ cApteczka ][ 2 ] = register_cvar( "dad_apteczka_ile", "20" );
	gCvar[ cApteczka ][ 3 ] = register_cvar( "dad_apteczka_limit", "100" );
	gCvar[ cKasa ][ 0 ] = register_cvar( "dad_kasa_ile", "300" );
	gCvar[ cKasa ][ 1 ] = register_cvar( "dad_kasa_ekran", "1" );
	gCvar[ cKasa ][ 2 ] = register_cvar( "dad_kasa_max", "16000" );
	gCvar[ cArmor ][ 0 ] = register_cvar( "dad_kamizelka_ile", "25" );
	gCvar[ cArmor ][ 1 ] = register_cvar( "dad_kamizelka_max", "100" );
	gCvar[ cArmor ][ 2 ] = register_cvar( "dad_kamizelka_min", "15" );
	gCvar[ cArmor ][ 3 ] =  register_cvar( "dad_kamizelka_usun_czas", "120" );	
	
	gMsg[ mScreenFade ] = get_user_msgid("ScreenFade");
	gMsg[ mItemPickUp ] = get_user_msgid( "ItemPickup" );
	
	register_event( "DeathMsg", "Event_DeatchMsg", "a" );
	register_forward( FM_Touch, "Forward_PlayerTouch" );
	register_logevent( "LogEvent_RoundStart", 2, "1=Round_Start" );
	register_touch( "FakeArmor", "player", "grabArmor" );
}

public plugin_precache( ) {
	for( new i = 0; i < sizeof( gPluginSounds ); i++ ) {
		precache_sound( gPluginSounds[ PluginSounds: i ] );
	}
	for( new i = 0; i < sizeof( gPluginModels ); i++ ) {
		precache_model( gPluginModels[ PluginModels: i ] );
	}
}

public client_connect( id ) can_pickup[ id ] = true;
public client_disconnect( id ) can_pickup[ id ] = false;
public reset_pick( id ) can_pickup[ id ] = true;

public Event_DeatchMsg( id ) {
	new victim = read_data( 2 );
	new killer = read_data( 1 );
	new Float:fOrigin[ 3 ];
	switch( random_num( 1,3 ) ) {
		case 1: {
			new money, Float:velo[ 3 ];
			
			if( killer && killer != victim ) {
				cs_set_user_money( killer,cs_get_user_money( killer ) - 300 );
			}
			
			money = get_pcvar_num( gCvar[ cKasa ][ 0 ] );
			if( cs_get_user_money( victim ) < money) {
				money = cs_get_user_money( victim );
				cs_set_user_money( victim,0 );
			}
			else {
				cs_set_user_money( victim,cs_get_user_money( victim ) - get_pcvar_num( gCvar[ cKasa ][ 0 ] ) );
			}
			make_money( victim, money, velo );
		}
		case 2:	{
			pev( victim, pev_origin, fOrigin );
			
			new ent = engfunc( EngFunc_CreateNamedEntity, engfunc( EngFunc_AllocString, "info_target" ) );
			
			fOrigin[ 2 ] -= 36; 
			
			engfunc( EngFunc_SetOrigin, ent, fOrigin );
			
			if( !pev_valid( ent ) ) {
				return PLUGIN_HANDLED;
			}
			
			set_pev( ent, pev_classname, gMedKitClassname );
			engfunc( EngFunc_SetModel, ent, gPluginModels[ pmApteczka ] );
			dllfunc( DLLFunc_Spawn, ent );
			set_pev( ent, pev_solid, SOLID_BBOX );
			set_pev( ent, pev_movetype, MOVETYPE_NONE );
			engfunc( EngFunc_SetSize, ent, Float:{ -23.160000, -13.660000, -0.050000 }, Float:{ 11.470000, 12.780000, 6.720000 } );
			engfunc( EngFunc_DropToFloor, ent );
			
			if( get_pcvar_num( gCvar[ cApteczka ][ 0 ] ) == 1 ) {
				set_rendering( ent, kRenderFxGlowShell, 255, 255, 255, kRenderFxNone, 27 );
			}
		}
		case 3: {
			get_armor_values( victim );
			
			if( g_ArmorType[ victim ] <= 0 || g_ArmorAmount[ victim ] <= float( get_pcvar_num( gCvar[ cArmor ][ 2 ] ) ) )
				return PLUGIN_CONTINUE;
				
			new origin[ 3 ];
			get_user_origin( victim,origin );
			for( new a = 0; a < 3; a++)
				fOrigin[ a ] = float( origin[ a ] );
			
			fArmor[ victim ] = create_entity( "info_target" );
			if( fArmor[ victim ] ) {
				entity_set_string( fArmor[ victim ],EV_SZ_classname,"FakeArmor" );
				entity_set_model( fArmor[ victim ], gPluginModels[ pmKamizelka ] );
				
				new Float:MinBox[ 3 ];
				new Float:MaxBox[ 3 ];
				MinBox[ 0 ] = -6.0;
				MinBox[ 1 ] = -8.0;
				MinBox[ 2 ] = -1.5;
				MaxBox[ 0 ] = 6.0;
				MaxBox[ 1 ] = 8.0;
				MaxBox[ 2 ] = 1.5;
				
				entity_set_vector( fArmor[ victim ], EV_VEC_mins, MinBox );
				entity_set_vector( fArmor[ victim ], EV_VEC_maxs, MaxBox );
				
				entity_set_int( fArmor[ victim ], EV_INT_solid, SOLID_BBOX );
				entity_set_int( fArmor[ victim ], EV_INT_movetype, MOVETYPE_TOSS );
				
				entity_set_int( fArmor[ victim ], EV_INT_iuser1, victim );
				entity_set_int( fArmor[ victim ], EV_INT_iuser3, g_ArmorType[ victim ] );
				entity_set_int( fArmor[ victim ], EV_INT_iuser4, floatround( g_ArmorAmount[ victim ] ) );
				
				entity_set_origin( fArmor[ victim ], fOrigin );
				set_task( float( get_pcvar_num( gCvar[ cArmor ][ 3 ] ) ), "removeThisArmor", fArmor[ victim ] );
			}
		}
	}
	return PLUGIN_CONTINUE;
}

public LogEvent_RoundStart( ) {
	new money_ent;
	while( ( money_ent = engfunc( EngFunc_FindEntityByString, money_ent, "classname", "pdm_money" ) ) != 0 ) {
		engfunc(EngFunc_RemoveEntity,money_ent);
	}
	new hkit = FM_NULLENT;
	while( ( hkit = fm_find_ent_by_class( hkit, gMedKitClassname ) ) ) {
		engfunc( EngFunc_RemoveEntity, hkit );
	}
	set_task( 5.0, "removeArmor",1111 );
}

public Forward_PlayerTouch( ent, id ) {
	if( !pev_valid( ent ) ) {
		return FMRES_IGNORED;
	}
	
	new classname[ MAX ];
	pev( ent, pev_classname, classname, charsmax( classname ) );
	
	if( !equal( classname, gMedKitClassname ) && !equal( classname, "pdm_money") ){
		return FMRES_IGNORED;
	}
	if ( equali( classname, gMedKitClassname ) ) { 
	new health = get_user_health( id );
	new cvarhealth = get_pcvar_num( gCvar[ cApteczka ][ 2 ] );
	new maxhealth = get_pcvar_num( gCvar[ cApteczka ][ 3 ] );
	
	if( health >= maxhealth ) {
		client_print( id, print_center, "Soorki ale masz %d HP. Wiec nie mozesz tego podnies :(! Musisz miec mniej jak %d HP aby to podnies :).", health, maxhealth ); 
		return FMRES_IGNORED;
	}
	
	set_hudmessage( 255, 0, 0, -1.0, 0.83, 2, 6.0, 3.0 );
	show_hudmessage( id, "Odzyskales %d HP", cvarhealth );
	
	fm_set_user_health( id, health + cvarhealth );
	emit_sound( id, CHAN_ITEM, gPluginSounds[ psApteczka ], VOL_NORM, ATTN_NORM ,0 , PITCH_NORM );
	
	message_begin( MSG_ONE_UNRELIABLE, gMsg[ mItemPickUp ], _, id );
	write_string( "item_healthkit" );
	message_end();
	
	if( get_pcvar_num( gCvar[ cApteczka ][ 1 ] ) == 1 ) {
		message_begin( MSG_ONE_UNRELIABLE, gMsg[ mScreenFade ] , _, id );
		write_short( 1<<10 );
		write_short( 1<<10 );
		write_short( 0x0000 );
		write_byte( 255 );
		write_byte( 0 );
		write_byte( 0 ); 
		write_byte( 75 );
		message_end();
	}
	
	engfunc( EngFunc_RemoveEntity, ent );
	}
	
	
	
	
	if ( equali( classname, "pdm_money" ) ) { 
		if( cs_get_user_money( id ) == get_pcvar_num( gCvar[ cKasa ][ 2 ] ) ) { 
			return FMRES_IGNORED;
		}
		else if( ( cs_get_user_money( id ) + moneybox[ ent ] ) > get_pcvar_num( gCvar[ cKasa ][ 2 ] ) ) { 
			cs_set_user_money( id, get_pcvar_num( gCvar[ cKasa ][ 2 ] ) );
		}
		else { 
			cs_set_user_money( id, cs_get_user_money( id ) + moneybox[ ent ] );
		}
		
		emit_sound( id, CHAN_ITEM, gPluginSounds[ psKasa ], VOL_NORM, ATTN_NORM ,0 , PITCH_NORM );
		
		if( pev_valid( ent ) )
			engfunc( EngFunc_RemoveEntity, ent );
		
		if( get_pcvar_num( gCvar[ cKasa ][ 1 ] ) ) { 
			message_begin( MSG_ONE, gMsg[ mScreenFade ], { 0, 0, 0 }, id );
			write_short( 1<<12 );
			write_short( 1<<12 );
			write_short( 1<<12 );
			write_byte( 0 );
			write_byte( 200 );
			write_byte( 0 );
			write_byte( 20 );
			message_end( );
		}
	}	
	return FMRES_IGNORED;
	
}

public make_money( id, money, Float:velo[ ] ) { 
	new moneybags = money/1000;
	new moneyleft = money;
	new Float:origin[ 3 ];
	new Float:angles[ 3 ];
	new Float:mins[ 3 ] = { -2.79, -0.0, -6.14 };
	new Float:maxs[ 3 ] = { 2.42, 1.99, 6.35 };
	
	if( ( moneybags * 1000 ) < money ) 
		moneybags++;
	
	for( new i = 0; i < moneybags; ++i ) { 
		new newent = engfunc( EngFunc_CreateNamedEntity, engfunc( EngFunc_AllocString, "info_target" ) );
		if( !is_user_alive( id ) ) { 
			velo[ 0 ] = random_float( 1.0, 150.0 );
			velo[ 1 ] = random_float( 1.0, 150.0 );
			velo[ 2 ] = random_float( 1.0, 150.0 );
		}
		else 
			velo[ 2 ] += 100;
		
		pev( newent, pev_angles, angles );
		angles[ 1 ] += random_num( 1, 360 );
		pev( id, pev_origin, origin );
		set_pev( newent, pev_origin, origin );
		set_pev( newent, pev_classname, "pdm_money" );
		engfunc( EngFunc_SetModel, newent, gPluginModels[ pmKasa ] );
		engfunc( EngFunc_SetSize, newent, mins, maxs );
		set_pev( newent, pev_angles, angles );
		set_pev( newent, pev_solid, SOLID_TRIGGER );
		set_pev( newent, pev_movetype, MOVETYPE_TOSS );
		set_pev( newent, pev_velocity, velo );
		engfunc( EngFunc_DropToFloor, newent );
		
		if( moneyleft == 0 )
			return FMRES_IGNORED;
		
		if( moneyleft < 1000 ) { 
			moneybox[ newent ] = moneyleft;
			moneyleft = 0;
			return FMRES_IGNORED;
		}
		moneyleft -= 1000;
		moneybox[ newent ] = 1000;
	}
	return FMRES_IGNORED;
}

public grabArmor( Toucher, Touched ) {
	if( is_user_alive( Touched ) && is_user_connected( Touched ) && entity_get_float( Touched,EV_FL_armorvalue ) < float( get_pcvar_num( gCvar[ cArmor ][ 1 ] ) ) ) {
		new owner = entity_get_int( Toucher, EV_INT_iuser1 );
		if( is_valid_ent( fArmor[ owner ] ) ) {
			get_armor_values( Touched );
			multiplier = float( get_pcvar_num( gCvar[ cArmor ][ 0 ] ) ) * 0.01;
			
			new armortype = entity_get_int( fArmor[ owner ], EV_INT_iuser3 );
			new armoramt = entity_get_int( fArmor[ owner ], EV_INT_iuser4 );
			
			if( g_ArmorType[ Touched ] == 0 ) {
				csset_user_armor( Touched, armoramt*multiplier, ( armortype==1?1:2 ) );
			}
			else if( g_ArmorType[ Touched ] == 1 && armortype == 1 ) {
				csset_user_armor( Touched, entity_get_float( Touched,EV_FL_armorvalue )+( armoramt*multiplier ), 1 );
			}
			else if( ( g_ArmorType[ Touched ] == 1 && armortype == 2 ) || g_ArmorType[ Touched ] == 2 ) {
				csset_user_armor( Touched, entity_get_float( Touched,EV_FL_armorvalue )+( armoramt*multiplier ), 2 );
			}
			
			if( entity_get_float( Touched,EV_FL_armorvalue ) > float( get_pcvar_num( gCvar[ cArmor ][ 1 ] ) ) ) {
				entity_set_float( Touched,EV_FL_armorvalue, float( get_pcvar_num( gCvar[ cArmor ][ 1 ] ) ) );
			}
			
			g_ArmorType[ Touched ] = armortype;
			remove_entity( fArmor[ owner ] );
			remove_task( fArmor[ owner ] );
		}
	}
	return PLUGIN_HANDLED;
}

public removeArmor( ) {
	new tempid = -1;
	while ( ( tempid = find_ent_by_class( tempid, "FakeArmor") ) > 0 )
		if( is_valid_ent( tempid) ) {
			remove_entity( tempid );
			remove_task( tempid );
	}
	return PLUGIN_CONTINUE;
}

public removeThisArmor( id ) {
	if( is_valid_ent( id ) ) {
		remove_entity( id );
	}
}

public get_armor_values( id ) {
	if( entity_get_float( id, EV_FL_armorvalue ) <= 0.0 )
		g_ArmorType[ id ] = 0;
	else {
		g_ArmorAmount[ id ] = entity_get_float( id, EV_FL_armorvalue );
		g_ArmorType[ id ] = get_pdata_int( id, 112 );
	}	
}

public csset_user_armor( id, Float:amount, type ) {
	entity_set_float( id, EV_FL_armorvalue, amount );
	set_pdata_int( id, 112, type );	
}
